# Introduction
This module uses a functionality already in Drupal Commerce and more
specifically the commerce_log module in order to allow the administrators to
send email notifications to their buyers.

# Features
When the administrator has an update for the order and wants this information to
be shared with the client, a comment should be added and the Send email about
the updates to the client? checkbox should be checked. Upon submission of the
comment form, the order receipt email is sent again by also passing it the
administrator's comment.

# Post-Installation
You should modify your `commerce-order-receipt.html.twig` file to contain the
variable `commerce_log_mailer_message` provided by this module.

# Additional Requirements
Requires the `commerce_log` module (included in Drupal Commerce but not always
enabled).

# Similar projects
You might also want to check [Commerce Mailer](https://www.drupal.org/project/commerce_email)
module in case you want to catch specific email events and send automated emails
about them to the clients or anyone else.
